//
//  TPMainMapViewController.m
//  Capybara 
//
//  Created by Kosuke Hata on 12/5/13.
//  Copyright (c) 2013 Animal Robo. All rights reserved.
//

#import "TPMainMapViewController.h"
#import "ARLocationManager.h"
#import "SocketIOPacket.h"
#import <Parse/Parse.h>
#import "UIViewController+MMDrawerController.h"
#import "ARSocketManager.h"

@implementation TPMainMapViewController
{
    BOOL drawerIsOpen;
    BOOL locationWasFound;
}

- (id)init
{
    self = [super init];
    if (self) {
        [[ARSocketManager sharedManager] start];

    }
    return self;
}

- (void)loadView
{
    UIView *view = [[UIView alloc] initWithFrame:[KHBase getCurrentCGRect]];
    view.backgroundColor = [UIColor lightGrayColor];
    
    KHBase *rect = [KHBase getBaseParameters];
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 44+self.y_start, rect.width, rect.height-44-self.y_start)];
    [self.mapView setShowsUserLocation:YES];
    [self.mapView setMapType:MKMapTypeStandard];
    [self.mapView setZoomEnabled:YES];
    [self.mapView setScrollEnabled:YES];
    self.mapView.delegate = self;
    
    [view addSubview:self.mapView];
    
    self.view = view;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = @"Map";
    
    [ARSocketManager sharedManager].delegate = self;
    
    PFUser *user = [PFUser currentUser];
    user[@"loggedIntoDirector"] = @YES;
    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
    }];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(openMenu:)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"location"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(findCurrentLocation:)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[ARLocationManager sharedLocation] start];
    [ARLocationManager sharedLocation].delegate = self;
    
    CLLocation *location = [[ARLocationManager sharedLocation] getLocation];
    [self.mapView setCenterCoordinate:location.coordinate animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[ARLocationManager sharedLocation] stop];
    locationWasFound = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Target Action Methods

- (void)openMenu:(id)sender
{
    if (!drawerIsOpen) {
        [self.mm_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
            drawerIsOpen = YES;
        }];
    } else {
        [self.mm_drawerController closeDrawerAnimated:YES completion:^(BOOL finished) {
            drawerIsOpen = NO;
        }];
    }
}

- (void)findCurrentLocation:(id)sender
{
    [self centerMapViewToCurrentLocation];
}

#pragma mark - helper methods

- (void)centerMapViewToCurrentLocation
{
    CLLocationCoordinate2D location;
    location.latitude = self.currentLocation.coordinate.latitude;
    location.longitude = self.currentLocation.coordinate.longitude;
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.01;
    span.longitudeDelta = 0.01;
    
    region.span = span;
    region.center = location;
    [self.mapView setRegion:region animated:YES];
}


#pragma mark - ARLocationManager delegate method

- (void)TPlocationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.currentLocation = [locations lastObject];
    
    
    NSString *latlong = [NSString stringWithFormat:@"%f, %f",
                         self.currentLocation.coordinate.latitude,
                         self.currentLocation.coordinate.longitude];
    
    NSDictionary *params = @{@"to": @"kosvke",
                             @"from":@"director",
                             @"message":latlong
                             };
    
    [[ARSocketManager sharedManager] sendEvent:@"message" withData:params];
    
//    NSLog(@"sending: %@", params);
    
    
    if (!locationWasFound) {
        [self centerMapViewToCurrentLocation];
        locationWasFound = YES;
    }
}

#pragma mark - MKMapView Delegate methods

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation
{
    
}

#pragma mark - socket.IO-objc delegate methods

- (void)socketIODidConnect:(SocketIO *)socket
{
    NSLog(@"socket.io connected.");
    
}

- (void)socketIO:(SocketIO *)socket onError:(NSError *)error
{
    NSLog(@"onError() %@", error);
}

- (void)socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)event
{
    NSLog(@"======== did receive event =======");
    
    if ([event.name isEqualToString:@"new_user"]) {
        NSLog(@"registering user");
        NSDictionary *params = @{@"user_id": @"socket",
                                 @"username": @"director",
                                 @"message": @"what up"
                                 };

        
        [[ARSocketManager sharedManager] sendEvent:@"register_user" withData:params];
    }
    
    if ([event.name isEqualToString:@"register_complete"]) {
        NSLog(@"%@", event.args[0]);
    }
    
    if ([event.name isEqualToString:@"received_message"]) {
        NSDictionary *dict = event.args[0];
        NSLog(@"%@ says: %@", dict[@"from"], dict[@"message"]);
    }
}

- (void)socketIO:(SocketIO *)socket didSendMessage:(SocketIOPacket *)packet
{
    
}

- (void) socketIODidDisconnect:(SocketIO *)socket disconnectedWithError:(NSError *)error
{
    NSLog(@"socket.io disconnected. did error occur? %@", error);
    [[ARSocketManager sharedManager] disconnect];
}

@end
