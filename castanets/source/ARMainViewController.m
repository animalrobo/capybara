//
//  ARMainViewController.m
//  Capybara
//
//  Created by Kosuke Hata on 3/3/14.
//  Copyright (c) 2014 animalrobo. All rights reserved.
//

#import "ARMainViewController.h"
#import "SocketIO.h"
#import "SocketIOPacket.h"
#import "ARUtility.h"
#import "KHButton.h"

#define kLandDroneColor [UIColor colorWithRed:0.5 green:0.1 blue:0.1 alpha:1.0]
#define kTakeOffDroneColor [UIColor colorWithRed:0.1 green:0.1 blue:0.5 alpha:1.0]

@interface ARMainViewController ()

@end

@implementation ARMainViewController {
    BOOL droneIsFlying;
}

- (id)init
{
    self = [super init];
    if (self) {
        //...
        droneIsFlying = NO;
        
    }
    return self;
}

#pragma mark - UIViewController Life Cycle Methods

- (void)loadView
{
    UIView *view = [[UIView alloc] init];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 320, 20)];
    label.backgroundColor = [UIColor darkGrayColor];
    label.text = [ARUtility getIPAddress:YES];
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    
    self.takeoffButton = [KHButton buttonWithColor:kTakeOffDroneColor withSize:CGSizeMake(320, 60) andTitle:@"TAKE OFF"];
    self.takeoffButton.delegate = self;
    [self.takeoffButton setButtonFont:@"HelveticaNeue-UltraLight" withSize:36];
    self.takeoffButton.frame = CGRectMake(0, 40, 320, 60);
    self.takeoffButton.tag = 1111;
    [view addSubview:self.takeoffButton];

    self.joystick = [[MFLJoystick alloc] initWithFrame:CGRectMake(96, 220, 128, 128)];
    [self.joystick setThumbImage:[UIImage imageNamed:@"joy_thumb.png"]
                 andBGImage:[UIImage imageNamed:@"stick_base.png"]];
    [self.joystick setDelegate:self];
    [self.joystick setAlpha:0.0];
    [view addSubview:self.joystick];
    
    self.landButton = [KHButton buttonWithColor:kLandDroneColor withSize:CGSizeMake(320, 60) andTitle:@"LAND"];
    self.landButton.delegate = self;
    self.landButton.tag = 2222;
    [self.landButton setButtonFont:@"HelveticaNeue-UltraLight" withSize:36];
    self.landButton.frame = CGRectMake(0, 568-60, 320, 60);
    [view addSubview:self.landButton];
    
    self.view = view;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [ARSocketManager sharedManager].delegate = self;
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - KHButton delegate view

- (void)buttonWasTouchedUpInside:(KHButton *)button
{
    if (button.tag == 1111) {
        [self takeOff];
    } else if (button.tag == 2222) {
        [self land];
    }
}

- (void)takeOff
{
    NSLog(@"taking off!");
    
    NSDictionary *params = @{@"user_id": @"socket",
                             @"from": @"director",
                             @"to": @"local",
                             @"message": @"takeoff"
                             };
    [[ARSocketManager sharedManager] sendEvent:@"drone" withData:params];
    
    droneIsFlying = YES;
    
    [UIView animateWithDuration:0.2 animations:^{
        self.joystick.alpha = 1.0;
    }];
}

- (void)land
{
    NSLog(@"landing!");

    NSDictionary *params = @{@"user_id": @"socket",
                             @"from": @"director",
                             @"to": @"local",
                             @"message": @"land"
                             };
    [[ARSocketManager sharedManager] sendEvent:@"drone" withData:params];

    droneIsFlying = NO;
    
    [UIView animateWithDuration:0.2 animations:^{
        self.joystick.alpha = 0.0;
    }];
}

#pragma mark - socket.IO-objc delegate methods

- (void)socketIODidConnect:(SocketIO *)socket
{
    NSLog(@"socket.io connected.");
}

- (void)socketIO:(SocketIO *)socket onError:(NSError *)error
{
    NSLog(@"onError() %@", error);
}

- (void)socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)event
{
    NSLog(@"======== did receive event =======");
    
    if ([event.name isEqualToString:@"new_user"]) {
        NSLog(@"registering user");
        NSDictionary *params = @{@"user_id": @"socket",
                                 @"username": @"director",
                                 @"message": @"register user"
                                 };
        
        [[ARSocketManager sharedManager] sendEvent:@"register_user" withData:params];
    }
    
    if ([event.name isEqualToString:@"register_complete"]) {
        NSLog(@"%@", event.args[0]);
    }
    
    if ([event.name isEqualToString:@"received_message"]) {
        NSDictionary *dict = event.args[0];
        NSLog(@"%@ says: %@", dict[@"from"], dict[@"message"]);
    }
}

- (void)socketIO:(SocketIO *)socket didSendMessage:(SocketIOPacket *)packet
{
    
}

- (void) socketIODidDisconnect:(SocketIO *)socket disconnectedWithError:(NSError *)error
{
    NSLog(@"socket.io disconnected. did error occur? %@", error);
    [[ARSocketManager sharedManager] disconnect];
}

#pragma mark MFLJoystick delegate method

- (void)joystick:(MFLJoystick *)aJoystick didUpdate:(CGPoint)dir
{
    
    NSDictionary *params = @{@"user_id": @"socket",
                             @"username": @"director",
                             @"x_coord": [NSString stringWithFormat:@"%f", dir.x],
                             @"y_coord": [NSString stringWithFormat:@"%f", dir.y]
                             };
    
    
    [[ARSocketManager sharedManager] sendEvent:@"joystick" withData:params];

    NSLog(@"%@", NSStringFromCGPoint(dir));
}

@end
