//
//  TPUsersListViewController
//  blackcard
//
//  Created by Kosuke Hata on 12/11/13.
//  Copyright (c) 2013 Animal Robo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface TPUsersListViewController : KHViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *users;

@end
