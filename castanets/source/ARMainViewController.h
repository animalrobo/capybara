//
//  ARMainViewController.h
//  Capybara
//
//  Created by Kosuke Hata on 3/3/14.
//  Copyright (c) 2014 animalrobo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ARSocketManager.h"
#import "MFLJoystick.h"
#import "KHButton.h"

@interface ARMainViewController : UIViewController <ARSocketManagerDelegate, KHButtonDelegate, JoystickDelegate>

@property (nonatomic, strong) KHButton *landButton;
@property (nonatomic, strong) KHButton *takeoffButton;
@property (nonatomic, strong) MFLJoystick *joystick;



@end
