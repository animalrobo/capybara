//
//  TPMainMapViewController.h
//  Capybara 
//
//  Created by Kosuke Hata on 12/5/13.
//  Copyright (c) 2013 Animal Robo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ARLocationManager.h"
#import <MapKit/MapKit.h>
#import "ARSocketManager.h"

@interface TPMainMapViewController : KHViewController <ARLocationManagerDelegate, MKMapViewDelegate, ARSocketManagerDelegate>
@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, strong) CLLocation *currentLocation;

@end
