//
//  ARLocationManager.h
//  Capybara 
//
//  Created by Kosuke Hata on 12/5/13.
//  Copyright (c) 2013 Animal Robo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol ARLocationManagerDelegate <NSObject>

- (void)TPlocationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations;

@end

@interface ARLocationManager : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, weak) id<ARLocationManagerDelegate> delegate;

+ (ARLocationManager *)sharedLocation;

- (void)start;
- (void)stop;

- (CLLocation *)getLocation;

@end
