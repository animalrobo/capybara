//
//  ARLocationManager.m
//  Capybara 
//
//  Created by Kosuke Hata on 12/5/13.
//  Copyright (c) 2013 Animal Robo. All rights reserved.
//

#import "ARLocationManager.h"

@interface ARLocationManager ()
@property (nonatomic, strong) CLLocationManager *locationManager;
@end

@implementation ARLocationManager
@synthesize locationManager, currentLocation;

#pragma mark - Singleton Pattern Setup

+ (ARLocationManager *)sharedLocation {
    static ARLocationManager *_sharedLocation = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedLocation = [[ARLocationManager alloc] init];
    });
    
    return _sharedLocation;
}

- (id)init {
    if (self = [super init]) {
        self.currentLocation = [[CLLocation alloc] init];
        self.locationManager = [[CLLocationManager alloc] init];
        [self.locationManager setDistanceFilter:kCLDistanceFilterNone];
        [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        self.locationManager.delegate = self;
    }
    return self;
}

- (void)start {
    [self.locationManager startUpdatingLocation];
}

- (void)stop {
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {

    if ([self.delegate respondsToSelector:@selector(TPlocationManager:didUpdateLocations:)]) {
        [self.delegate TPlocationManager:manager didUpdateLocations:locations];
    }
}

- (CLLocation *)getLocation {
    return self.currentLocation;
}

@end
