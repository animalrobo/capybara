//
//  TPCheckListViewController.h
//  Capybara
//
//  Created by Kosuke Hata on 2/10/14.
//  Copyright (c) 2014 Animal Robo. All rights reserved.
//

#import "KHViewController.h"

@interface TPCheckListViewController : KHViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *preFlightList;
@property (nonatomic, strong) NSArray *beforeLeavingList;

@end
