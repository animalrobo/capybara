//
//  TPCheckListViewController.m
//  Capybara
//
//  Created by Kosuke Hata on 2/10/14.
//  Copyright (c) 2014 Animal Robo. All rights reserved.
//

#import "TPCheckListViewController.h"
#import "UIViewController+MMDrawerController.h"

@interface TPCheckListViewController ()

@end

@implementation TPCheckListViewController {
    BOOL drawerIsOpen;
    BOOL resetValues;
}

- (id)init
{
    self = [super init];
    if (self) {
        drawerIsOpen = NO;
        resetValues = NO;
        
        self.beforeLeavingList = @[
                               @"FPV Tx Battery Charged",
                               @"FPV Rx Battery Charged",
                               @"Turnigy 4500mAh 25-50C Charged"
                               @"Turnigy 4500mAh 35-70C Charged"
                               @"APM 3DR Radio 915Mhz + USB A cord",
                               @"APM Properly Functioning",
                               @"Spare Props 10x5",
                               @"Spare Props 11x6"
                               ];
        
        self.preFlightList = @[
                                @"Turn on 9XR Transmitter",
                                @"Plug-in Tiger 900mAh into FPV Tx",
                                @"Plug-in Main 4500mAh Battery",
                                @"Plug-in Turnigy 2200mAh Monitor Rx",
                                @"Turn on FPV, check it works",
                                @"Plug-in USB to Radio 915MHz",
                                @"Turn on Droid/Mission Planner",
                                @"Connect Radio to Drone",
                                @"Tighten Prop Nuts",
                                @"Arm Drone"
                                ];
    }
    return self;
}

#pragma mark - UIViewController Life Cycle Methods

- (void)loadView
{
    UIView *view = [[UIView alloc] initWithFrame:[KHBase getCurrentCGRect]];
    view.backgroundColor = [UIColor whiteColor];
    
    self.tableView = [[UITableView alloc] initWithFrame:[KHBase getCurrentCGRect]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [view addSubview:self.tableView];
    
    self.view = view;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    self.navigationItem.title = @"Y6 Pre-Flight";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(openMenu:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"clear"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(clear:)];


}

#pragma mark - Target Action Methods

- (void)openMenu:(id)sender
{
    if (!drawerIsOpen) {
        [self.mm_drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
            drawerIsOpen = YES;
        }];
    } else {
        [self.mm_drawerController closeDrawerAnimated:YES completion:^(BOOL finished) {
            drawerIsOpen = NO;
        }];
    }
}

- (void)clear:(id)sender
{
    resetValues = YES;
    [self.tableView reloadData];
    resetValues = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewCell Data Source Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.preFlightList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"CellID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
    }
    
    cell.textLabel.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:18];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.text = self.preFlightList[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (resetValues) {
        cell.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

@end
