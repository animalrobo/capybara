//
//  ARUtility.h
//  Capybara
//
//  Created by Kosuke Hata on 3/4/14.
//  Copyright (c) 2014 animalrobo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARUtility : NSObject

+ (NSString *)getIPAddress:(BOOL)preferIPv4;

@end
