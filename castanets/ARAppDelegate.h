//
//  ARAppDelegate.h
//  Capybara 
//
//  Created by Kosuke Hata on 12/4/13.
//  Copyright (c) 2013 Animal Robo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
