//
//  UIColor+addVeryLightGray.h
//  egs-ipad
//
//  Created by Kosuke Hata on 1/11/13.
//  Copyright (c) 2013 Circle Time. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (addVeryLightGray)

+ (UIColor *) veryLightGray;

@end
