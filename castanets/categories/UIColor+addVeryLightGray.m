//
//  UIColor+addVeryLightGray.m
//  egs-ipad
//
//  Created by Kosuke Hata on 1/11/13.
//  Copyright (c) 2013 Circle Time. All rights reserved.
//

#import "UIColor+addVeryLightGray.h"

@implementation UIColor (addVeryLightGray)


+ (UIColor *) veryLightGray {

    UIColor *veryLightGray = [UIColor colorWithRed:242.5/255.0 green:242.5/255.0 blue:242.5/255.0 alpha:1.0];
    
    return veryLightGray;
}

@end

