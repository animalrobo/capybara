//
//  main.m
//  Capybara 
//
//  Created by Kosuke Hata on 12/4/13.
//  Copyright (c) 2013 Animal Robo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ARAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ARAppDelegate class]));
    }
}
